#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

// unclicked and empty inside
#define UNC 0
// unclicked but bomb inside
#define BOM 1
// clicked (empty)
#define CLI 2
// flagged
#define FLA 3

int **map;
int mapSize;
int mapQuantBombs;
int animation = 0;
int animation_sleep = 0;

#define MINPERCBOMBS 0.05
#define MAXPERCBOMBS 0.25

#define MINBOUND 100

/*
 *  The map row or column count is calculated using the
 *  square root of the level number plus a minumin bound
 *  to make the levels always start at 10x10.
 */
int getMapSize(int in){
  return (int)sqrt(in+MINBOUND);
}

/*
 *  Sets a seed for the random number generation based on
 *  the level number so the results are always the same for that level.
 *  Random numbers are used in the bomb placement inside the map.
 */
int setSeed(int in){
  unsigned i = (sqrt(in+MINBOUND)*1000000.0);
  srand(i);
  return i;
}

/*
 *  Returns the ammount of bombs that are in a passed level.
 *  Takes the sqrt of the (level number + the Minimum Bound (MINBOUND))
 *  Uses the decimal part of the result as a percentage between the MAXPERCBOMBS and MINPERCBOMBS.
 */
int getQuantBombs(int in){
  float perc = sqrt(in+MINBOUND)-getMapSize(in);
  float percBombs = MINPERCBOMBS + ((MAXPERCBOMBS-MINPERCBOMBS)*perc);
  return percBombs*pow(getMapSize(in),2);
}

/*
 *  The bombs' spots are calculaed always the same for the desired level
 *  because the seed for the random numbers is based on the level number.
 *  The random number is modded with the total count of spots in the array.
 *  There's no verification for repeated random numbers. Deal with it.
 */
void putNextBomb(int size){
  map[(rand()%(size))][(rand()%(size))] = BOM;
}

/*
 *  Calculates how many bombs are around a cell by its coordinates.
 */
int getBombsAround(int r, int c){
  int count=0;
  // -- middle of map
  if(r>0&&r<mapSize-1 && c>0&&c<mapSize-1){
    if(map[r-1][c-1]==BOM)count++;
    if(map[r-1][c]==BOM)  count++;
    if(map[r-1][c+1]==BOM)count++;
    if(map[r][c-1]==BOM)  count++;
    if(map[r][c+1]==BOM)  count++;
    if(map[r+1][c-1]==BOM)count++;
    if(map[r+1][c]==BOM)  count++;
    if(map[r+1][c+1]==BOM)count++;
    return count;
  } else {
    // -- corners
    if(r==0&&c==0){
      if(map[r][c+1]==BOM)  count++;
      if(map[r+1][c]==BOM)  count++;
      if(map[r+1][c+1]==BOM)count++;
      return count;
    }
    if(r==0&&c==mapSize-1){
      if(map[r][c-1]==BOM)  count++;
      if(map[r+1][c-1]==BOM)count++;
      if(map[r+1][c]==BOM)  count++;
      return count;
    }
    if(r==mapSize-1&&c==0){
      if(map[r-1][c]==BOM)  count++;
      if(map[r-1][c+1]==BOM)count++;
      if(map[r][c+1]==BOM)  count++;
    }
    if(r==mapSize-1&&c==mapSize-1){
      if(map[r-1][c-1]==BOM)count++;
      if(map[r-1][c]==BOM)  count++;
      if(map[r][c-1]==BOM)  count++;
      return count;
    }
    // -- walls
    if(r==0&&c>0&&c<mapSize-1){ // top
      if(map[r][c-1]==BOM)  count++;
      if(map[r][c+1]==BOM)  count++;
      if(map[r+1][c-1]==BOM)count++;
      if(map[r+1][c]==BOM)  count++;
      if(map[r+1][c+1]==BOM)count++;
      return count;
    }
    if(r==mapSize-1&&c>0&&c<mapSize-1){ // bottom
      if(map[r-1][c-1]==BOM)count++;
      if(map[r-1][c]==BOM)  count++;
      if(map[r-1][c+1]==BOM)count++;
      if(map[r][c-1]==BOM)  count++;
      if(map[r][c+1]==BOM)  count++;
      return count;
    }
    if(r>0&&r<mapSize-1&&c==0){ // left
      if(map[r-1][c]==BOM)  count++;
      if(map[r-1][c+1]==BOM)count++;
      if(map[r][c+1]==BOM)  count++;
      if(map[r+1][c]==BOM)  count++;
      if(map[r+1][c+1]==BOM)count++;
      return count;
    }
    if(r>0&&r<mapSize-1&&c==mapSize-1){ // right
      if(map[r-1][c-1]==BOM)count++;
      if(map[r-1][c]==BOM)  count++;
      if(map[r][c-1]==BOM)  count++;
      if(map[r+1][c-1]==BOM)count++;
      if(map[r+1][c]==BOM)  count++;
      return count;
    }
  }
}

/*
 *  par:  0 dont show bombs
 *        1 show bombs
 */
void printMap(int par){
  int i, j;
  // header begin
  printf("\t");
  for(i=0; i<mapSize; i++){
    printf("%02d  ",i);
  }
  printf("\n\n");
  // header end
  
  for(i=0; i<mapSize; i++){
    printf("%02d\t",i); // row number
    for(j=0; j<mapSize; j++){
      switch(map[i][j]){
        case UNC: printf("•"); break;
        case CLI: printf("%d", getBombsAround(i,j)); break;
        case BOM: par==0?printf("•"):printf("X"); break;
        case FLA: printf("F"); break;
      }
      printf("   ");
    }
    printf("\n\n"); // end of row line break
  }
}

/*
 *  Sets up the game based on the level number passed.
 *  - Calculates the map size;
 *  - Allocates the game map based on the map size;
 *  - Calculates the number os bombs in the desired map;
 *  - Sets the random number generator's seed for the bomb placement;
 *  - Fills the map with UNCLICKED;
 *  - Puts the bombs on the map.
 */
void setupMap(int in){
  mapSize = getMapSize(in);
  map = (int**)malloc(sizeof(int*)*(mapSize));
  int i,j;
  for(i=0; i<mapSize; i++){
    map[i] = (int*)malloc(sizeof(int)*(mapSize));
  }
  mapQuantBombs = getQuantBombs(in);
  setSeed(in);
  
  for(i=0; i<mapSize; i++){
    for(j=0; j<mapSize; j++){
      map[i][j]=UNC;
    }
  }
  for(i=0; i<mapQuantBombs; i++){
    putNextBomb(mapSize);
  }
  
}

void clrs(){
  if (system("CLS")) system("clear");
}

/*
 *  Recursively floods the click on the map to open all 0-cells around a clicked 0-cell.
 *  Returns at non-0-cells.
 *  A 0-cell is a cell that has no adjacent bombs.
 */
void floodClick(int r, int c){
  if(r<0 || r>=mapSize || c<0 || c>=mapSize) return;
  
  if(map[r][c]==CLI || map[r][c]==BOM) return;
  
  if(map[r][c]==UNC) map[r][c]=CLI; // set UNCLICKED cells that were tested to CLICKED.

  if(animation)
  {
	printMap(0);
	usleep(animation_sleep);
	clrs();
  }
  
  if(getBombsAround(r,c)==0){ // if the cell shows 0, flood around it.
    floodClick(r-1,c-1);
    floodClick(r-1,c);
    floodClick(r-1,c+1);
    floodClick(r,c-1);
    floodClick(r,c+1);
    floodClick(r+1,c-1);
    floodClick(r+1,c);
    floodClick(r+1,c+1);
  } else return;  // if the cell shows !=0, stop there and return.
}

int mapAllClicked(){
  int i,j;
  for(i=0; i<mapSize; i++){
    for(j=0; j<mapSize; j++){
      if(map[i][j]==UNC) return 0;
    }
  }
  return 1;
}

int main (int argc, char const *argv[]){
  if(argc<2){
    printf("Usage: input level number [0,+inf) eZ\nfor animations add a sleep time in positive ms after the level number (requires unistd)\n");
    return 0;
  }

  if(atoi(argv[1])<0)
  {
	printf("%s\n", "Level numbers are [0,+inf) eZ\n");
	return 0;
  }

  setupMap(atoi(argv[1]));

  if(argc==3){
	animation = 1;
	sscanf(argv[2],"%d",&animation_sleep);
  }

  printMap(1);
  clrs();
  printMap(0);
  
  int row, col;
  printf("Row Col: ");
  scanf("%d %d", &row, &col);
  while(1){
    if(row>mapSize||col>mapSize||row<0||col<0){
      printf("Invalid coordinates.\n");
    } else {
      clrs();
      if(map[row][col]==BOM){
        printf("%d %d",row,col);
        printMap(1); // show map
        printf("YOU LOSE.\n");
        break;
      }else
      if(map[row][col]==UNC){
        floodClick(row,col);
        printf("%d %d",row,col);
        printMap(0);
        if(mapAllClicked()){
          printf("YOU WIN!\n");
          break;
        }
      }
    }
    printf("Row Col: ");
    scanf("%d %d", &row, &col);
  }
  return 0;
}